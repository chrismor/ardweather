/*
 * Note:  You need to create the ElasticSearch Index and Mappings beforehand
 * curl -XPUT 'http://host:9200/test?pretty' -H 'Content-Type: application/json' -d'
{
    "mappings" : {
        "reading" : {
            "properties" : {
                "deviceid" : { "type" : "text" },
                "humidity" : { "type" : "float" },
                "temperature" : { "type" : "float" },
                "pressure" : { "type" : "float" },
                "dewpoint" : { "type" : "float" },
                "timestamp" : { "type" : "date" } 
            }
        }
    }
}
'

 */

/* *  Simple HTTP get webclient test
 */
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <TimeLib.h>
#include <WiFiUdp.h>

//#define _DEBUG

#define DHT11PIN 4

static const char ntpServerName[] = "us.pool.ntp.org";  
// Setup your wifi SSID and password here.
const char* ssid     = "MyWiFi";
const char* password = "Y0uwi11Nev3rGu355";
const int timeZone = 0;  // UTC
// Variables needed for NTP
// Elasticsearch needs us to generate timestamps for the data in order to make date histograms in Kibana.
WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
time_t getNtpTime();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);
// This is the IP address, or DNS name of my Elasticsearch instance.
const char* host = "ec2-52-62-173-57.ap-southeast-2.compute.amazonaws.com";
const int port = 9200;
int motion;
// Variables
float temperature;
float humidity;
String timestamp;
time_t start_time;
uint32_t t_ms;
uint32_t start_mills;
String run_mills;
int milis_chars;

byte DHTtemp,DHThum;
int DHTstatus=0;
int temp,hum;   //variables to log

byte mac[6];
String MacAddress;


//set pin to output, set high for idle state
void DHTSetup(){
  pinMode(DHT11PIN,OUTPUT);
  digitalWrite(DHT11PIN,HIGH);
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  delay(100);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");  
#ifdef _DEBUG
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Setting up NTP");
#endif
  Udp.begin(localPort);
#ifdef _DEBUG
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  Serial.println("waiting for sync");
#endif
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
  WiFi.macAddress(mac);
  MacAddress += (String)mac[5];
  MacAddress += (String)mac[4];
  MacAddress += (String)mac[3];
  MacAddress += (String)mac[2];
  MacAddress += (String)mac[1];
  MacAddress += (String)mac[0];
  start_time = now();
#ifdef _DEBUG
  Serial.println("Pressure Sensor Test"); Serial.println("");
#endif
  /* Initialise the sensor */
  DHTSetup();
}

int DHT11(){                    //returns 1 on ok, 0 on fail (eg checksum, data not received)
  unsigned int n[83];           //to store bit times
  byte p=0;                     //pointer to current bit
  unsigned long t;              //time
  byte old=0;
  byte newd;
  for(int i=0;i<83;i++){n[i]=0;}
  digitalWrite(DHT11PIN,LOW);   //start signal
  delay(25);
  digitalWrite(DHT11PIN,HIGH);
  delayMicroseconds(20);
  pinMode(DHT11PIN,INPUT);
  delayMicroseconds(40);
  t=micros()+10000L;
  while((micros()<t)&&(p<83)){    //read bits
    newd=digitalRead(DHT11PIN);
    if(newd!=old){
      n[p]=micros();
      p++;
      old=newd;
    }
  }
  pinMode(DHT11PIN,OUTPUT);      //reset for next cycle
  digitalWrite(DHT11PIN,HIGH);
  if(p!=83){return 0;}           //not a valid datastream
  byte data[5]={0,0,0,0,0};
  for(int i=0;i<40;i++){         //store data in array
    if(n[i*2+3]-n[i*2+2]>50){data[i>>3]=data[i>>3]|(128>>(i&7));}
  }
  byte k=0;     //checksum
  for(int i=0;i<4;i++){k=k+data[i];}
  if((k^data[4])){return 0;}      //checksum error
  DHTtemp=data[2];                //temperature
  DHThum=data[0];                 //humidity
  return 1;                       //data valid
}

void getvalue(){                  //put subroutine for getting data to log here
  DHTtemp=0;                      //zero values to detect errors
  DHThum=0;
  DHTstatus=DHT11();              //DHTstatus=0 if error
  temp=DHTtemp;                   //DHT11() loads temp into DHTtemp      
  hum=DHThum;                     //DHT11() loads humidity into DHThum
#ifdef _DEBUG
  Serial.print("Temp=");
  Serial.print(temp);
  Serial.print(", Humidity=");
  Serial.println(hum);
#endif
}

double getDewPoint(double temperature, double humidity) {
  double gamma = log(humidity / 100) + ((17.62 * temperature) / (243.5 + temperature));
}

void loop() {    
  // Measure pressure & temperature from BMP sensor
  // Modified from https://learn.adafruit.com/bmp085/using-the-bmp085-api-v2
  float temperature;
  getvalue();
  
  // Use WiFiClient class to create TCP connections, connect to the Elasticsearch instance.
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return;
  }
  run_mills = String(millis());
  milis_chars = run_mills.length();
  
  // To generate a millisecond unix timestamp, we first get the second timestamp, and add to it, the last three characters of the arduino/relative millisecond timestamp
  timestamp = String(now()) + run_mills.charAt(milis_chars-3) + run_mills.charAt(milis_chars-2) + run_mills.charAt(milis_chars-1);
  
  // With such a simple document, we're just going to use a string to generate the JSON to send to Elasticsearch
  String data = "{\"deviceid\": \""+ MacAddress + "\",\"temperature\": "+String(temp)+", \"humidity\": "+String(hum)+", \"pressure\": "+String(0)+", \"dewpoint\": "+String(getDewPoint(temp, hum))+", \"timestamp\": "+ timestamp +"}";
  // We can inspect the data being sent over the Serial line, in the Arduino IDE.
#ifdef _DEBUG
  Serial.println(data);
#endif
  // We now create a URI for the request
  // This is the index of the Elasticsearch document we're creating
//  String url = "/weather/reading";
  String url = "/test/reading";
  //
  digitalWrite(BUILTIN_LED,LOW); 
  client.print(String("POST ") + url + " HTTP/1.1\r\n" +
               // If you're using Shield, you'll need to generate an authentication header
               "Content-Length: " + data.length() + "\r\n" +
               "\r\n" + data);
  // We need this delay in here to give the WiFi Time
  delay(50);
  digitalWrite(BUILTIN_LED,HIGH);
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  Serial.println();
  delay(60000);
}

/* Copied from https://github.com/PaulStoffregen/Time/blob/master/examples/TimeNTP_ESP8266WiFi/TimeNTP_ESP8266WiFi.ino#L99 */
/*-------- NTP code ----------*/
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}
// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}
